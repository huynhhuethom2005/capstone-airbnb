import React from "react";
import { useSelector } from "react-redux";
import { localServ } from "../../service/localService";
import { DownOutlined } from "@ant-design/icons";
import { Button, Dropdown, Space, message } from "antd";
import { useNavigate } from "react-router-dom";

export default function Usermenu() {
  let navigate = useNavigate();

  let userLogin = useSelector((state) => {
    return state.userSlice.userLogin;
  });

  console.log("userLogin: ", userLogin);

  let handleLogout = () => {
    localServ.remove();
    window.location.href = "/login";
  };

  let handleRegister = () => {
    localServ.remove();
    window.location.href = "/register";
  };

  const handleMenuClick = (e) => {
    switch (e.key) {
      case "1":
        handleRegister();
        break;
      case "2":
        navigate("/login");
        break;
    }
  };

  const handleMenuClickUser = (e) => {
    switch (e.key) {
      case "2":
        navigate(`/tickets-by-user/${userLogin.id}`);
        break;
      case "3":
        navigate(`/personal-info/${userLogin.id}`);
        break;
      case "7":
        handleLogout();
        break;
    }
  };

  const items = [
    {
      label: "Đăng ký",
      key: "1",
    },
    {
      label: "Đăng nhập",
      key: "2",
    },
    {
      label: "Cho thuê nhà",
      key: "3",
    },
    {
      label: "Tổ chức trải nghiệm",
      key: "4",
    },
    {
      label: "Trợ giúp",
      key: "5",
    },
  ];

  const itemsUser = [
    {
      label: "Tin nhắn",
      key: "1",
    },
    {
      label: "Chuyến đi",
      key: "2",
    },
    {
      label: "Thông tin cá nhân",
      key: "3",
    },
    {
      type: "divider",
    },
    {
      label: "Cho thuê nhà",
      key: "4",
    },
    {
      label: "Tổ chức trải nghiệm",
      key: "5",
    },
    {
      label: "Trợ giúp",
      key: "6",
    },
    {
      type: "divider",
    },
    {
      label: "Đăng xuất",
      key: "7",
    },
  ];

  const menuProps = {
    items,
    onClick: handleMenuClick,
  };

  const menuPropsUser = {
    items: itemsUser,
    onClick: handleMenuClickUser,
  };

  let renderContent = () => {
    if (userLogin != null) {
      return (
        <div className="relative flex items-center justify-end">
          <Dropdown
            className="relative inline-flex items-center rounded-full border px-2 hover:shadow-lg"
            menu={menuPropsUser}
          >
            <Button>
              <Space>
                <div className="pl-1">
                  <svg
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    role="presentation"
                    focusable="false"
                    style={{
                      display: "block",
                      fill: "none",
                      height: 16,
                      width: 16,
                      stroke: "currentcolor",
                      strokeWidth: 3,
                      overflow: "visible",
                    }}
                  >
                    <g fill="none" fillRule="nonzero">
                      <path d="m2 16h28" />
                      <path d="m2 24h28" />
                      <path d="m2 8h28" />
                    </g>
                  </svg>
                </div>

                <div className="block h-10 w-12 pl-4">
                  <svg
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    role="presentation"
                    focusable="false"
                    style={{
                      display: "block",
                      height: "100%",
                      width: "100%",
                      fill: "currentcolor",
                    }}
                  >
                    <path d="m16 .7c-8.437 0-15.3 6.863-15.3 15.3s6.863 15.3 15.3 15.3 15.3-6.863 15.3-15.3-6.863-15.3-15.3-15.3zm0 28c-4.021 0-7.605-1.884-9.933-4.81a12.425 12.425 0 0 1 6.451-4.4 6.507 6.507 0 0 1 -3.018-5.49c0-3.584 2.916-6.5 6.5-6.5s6.5 2.916 6.5 6.5a6.513 6.513 0 0 1 -3.019 5.491 12.42 12.42 0 0 1 6.452 4.4c-2.328 2.925-5.912 4.809-9.933 4.809z" />
                  </svg>
                </div>
              </Space>
            </Button>
          </Dropdown>
        </div>
      );
    } else {
      return (
        <div className="relative flex items-center justify-end">
          <Dropdown
            className="relative inline-flex items-center rounded-full border px-2 hover:shadow-lg"
            menu={menuProps}
          >
            <Button>
              <Space>
                <div className="pl-1">
                  <svg
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    role="presentation"
                    focusable="false"
                    style={{
                      display: "block",
                      fill: "none",
                      height: 16,
                      width: 16,
                      stroke: "currentcolor",
                      strokeWidth: 3,
                      overflow: "visible",
                    }}
                  >
                    <g fill="none" fillRule="nonzero">
                      <path d="m2 16h28" />
                      <path d="m2 24h28" />
                      <path d="m2 8h28" />
                    </g>
                  </svg>
                </div>

                <div className="block h-10 w-12 pl-4">
                  <svg
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    role="presentation"
                    focusable="false"
                    style={{
                      display: "block",
                      height: "100%",
                      width: "100%",
                      fill: "currentcolor",
                    }}
                  >
                    <path d="m16 .7c-8.437 0-15.3 6.863-15.3 15.3s6.863 15.3 15.3 15.3 15.3-6.863 15.3-15.3-6.863-15.3-15.3-15.3zm0 28c-4.021 0-7.605-1.884-9.933-4.81a12.425 12.425 0 0 1 6.451-4.4 6.507 6.507 0 0 1 -3.018-5.49c0-3.584 2.916-6.5 6.5-6.5s6.5 2.916 6.5 6.5a6.513 6.513 0 0 1 -3.019 5.491 12.42 12.42 0 0 1 6.452 4.4c-2.328 2.925-5.912 4.809-9.933 4.809z" />
                  </svg>
                </div>
              </Space>
            </Button>
          </Dropdown>
        </div>
      );
    }
  };
  return <div className="block z-10">{renderContent()}</div>;
}
