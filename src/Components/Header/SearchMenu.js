import React, { useEffect, useState } from "react";
import { vitriService } from "../../service/vitriService";
import { Select, message } from "antd";
import { useNavigate } from "react-router-dom";

export default function SearchMenu() {
  const [toggle, setToggle] = useState(true);
  const [items, setItems] = useState([]);
  let selected = null;

  let navigate = useNavigate();

  useEffect(() => {
    vitriService
      .getVitri()
      .then((res) => {
        console.log(res.data.content);
        let newItems = [];
        res.data.content.map((item) =>
          newItems.push({
            label: item.tenViTri,
            value: item.id,
          })
        );
        console.log("items: ", newItems);
        setItems(newItems);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleOnChange = (value) => {
    selected = value;
  };

  return (
    <>
      <div
        onClick={() => {
          setToggle(!toggle);
        }}
        className="flex flex-wrap justify-center items-center relative z-20"
        style={{ flex: "1 1 45%" }}
      >
        <div className="flex flex-wrap justify-center items-center">
          <a className="mx-2" href="/">
            Chỗ ở
          </a>
          <a className="mx-2" href="/">
            Trải nghiệm
          </a>
          <a className="mx-2" href="/experience">
            Trải nghiệm trực tuyến
          </a>
        </div>

        <div
          className={`absolute flex flex-wrap px-3 py-1.5 rounded-full shadow-lg border bg-white justify-center items-center cursor-pointer z-20 ${
            toggle ? "" : "hidden"
          }`}
        >
          <div className="font-medium border-r pr-2">Địa điểm bất kỳ</div>
          <div className="font-medium border-r pr-2 pl-2">tuần bất kỳ</div>
          <div className="font-normal pr-2 pl-2">Thêm khách</div>
          <div className="inline-flex w-8 h-8 justify-center items-center bg-rose-500 rounded-full text-white">
            <svg
              viewBox="0 0 32 32"
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              role="presentation"
              focusable="false"
              style={{
                display: "block",
                fill: "none",
                height: 12,
                width: 12,
                stroke: "currentcolor",
                strokeWidth: "5.33333",
                overflow: "visible",
              }}
            >
              <g fill="none">
                <path d="m13 24c6.0751322 0 11-4.9248678 11-11 0-6.07513225-4.9248678-11-11-11-6.07513225 0-11 4.92486775-11 11 0 6.0751322 4.92486775 11 11 11zm8-3 9 9" />
              </g>
            </svg>
          </div>
        </div>
      </div>

      <div
        className={`absolute top-0 left-0 w-screen bg-white transition-all duration-300 pb-3 z-10 ${
          toggle ? "" : "shadow-lg"
        }`}
        style={{ paddingTop: "4.4rem" }}
      >
        <div className="flex flex-wrap justify-center items-center">
          <div
            className={`flex flex-wrap justify-center items-center relative  transition-all duration-300  ${
              toggle ? "h-0 overflow-hidden" : "h-16 border rounded-full"
            }`}
          >
            <div
              className={`px-5 py-2 hover:bg-gray-300 rounded-full h-full flex flex-wrap justify-center items-center ${
                toggle ? " " : "border bg-white hover:bg-white shadow-lg"
              }`}
            >
              <label
                htmlFor="checkInDate"
                className="block text-sm font-medium text-gray-900 dark:text-gray-300"
              >
                Địa điểm
              </label>
              <div className="w-48 bg-transparent outline-none select_location css-b62m3t-container">
                <Select
                  name="location"
                  bordered={false}
                  style={{
                    width: "100%",
                  }}
                  onChange={handleOnChange}
                  options={items}
                />
              </div>
            </div>
            <div className="hidden sm:block py-2 px-5 hover:bg-gray-300 rounded-full overflow-hidden h-full">
              <label
                htmlFor="checkInDate"
                className="block text-sm font-medium text-gray-900 dark:text-gray-300"
              >
                Nhận phòng
              </label>
              <input
                name="checkIn"
                type="date"
                id="checkInDate"
                className="bg-transparent outline-none"
                placeholder="Thêm ngày"
              />
            </div>
            <div className="hidden sm:block py-2 px-5 hover:bg-gray-300 rounded-full overflow-hidden h-full">
              <label
                htmlFor="checkOutDate"
                className="block text-sm font-medium text-gray-900 dark:text-gray-300"
              >
                Trả phòng
              </label>
              <input
                name="checkOut"
                type="date"
                id="checkOutDate"
                className="bg-transparent outline-none"
                placeholder="Thêm ngày"
              />
            </div>
            <div className="hidden sm:block py-2 pl-7 pr-5 hover:bg-gray-300 rounded-full overflow-hidden h-full">
              <label
                htmlFor="guest"
                className="block text-sm font-medium text-gray-900 dark:text-gray-300"
              >
                Khách
              </label>
              <input
                name="guest"
                type="number"
                id="guest"
                className="bg-transparent outline-none"
                placeholder="Thêm khách"
              />
            </div>
            <button 
            onClick={() => {
              const location = selected;
              console.log(location);
              navigate(`/roombycity/${location}`)
            }}
            className="text-white bg-red-700 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 absolute right-0">
              Tìm kiếm
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
