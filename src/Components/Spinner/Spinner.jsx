import { Spin } from 'antd'
import React from 'react'

export default function Spinner({ isLoading }) {
    return isLoading ? (
        <>
            <Spin style={{ background: "#07BCF6" }} className='opacity-60 w-screen h-screen fixed top-0 left-0 flex justify-center items-center z-50' />
        </>
    ) : <></>
}
