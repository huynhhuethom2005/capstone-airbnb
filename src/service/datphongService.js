import { https } from "./config"

export const datPhongService = {
    postDatPhong: (dataDatPhong) => {
        return https.post('/api/dat-phong', dataDatPhong)
    }
}