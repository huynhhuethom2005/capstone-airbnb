import { https } from "./config"

export const vitriService = {
    getVitri: () => {
        return https.get('/api/vi-tri')
    },

    getViTriId: (id) => {
        return https.get(`/api/vi-tri/${id}`)
    },

    deleteViTri: (id) => {
        return https.delete(`/api/vi-tri/${id}`)
    },

} 