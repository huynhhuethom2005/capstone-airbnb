import axios from "axios";

export const BASE_URL = "https://airbnbnew.cybersoft.edu.vn";

export const DOMAIN_URL = "http://localhost:3000"

const TokenCybersoft =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U";

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjI2NDIiLCJlbWFpbCI6ImFkbWluMDVAZ21haWwuY29tIiwicm9sZSI6IkFETUlOIiwibmJmIjoxNjgzNTMwMTk4LCJleHAiOjE2ODQxMzQ5OTh9.hEEh4siztem0aAqSoqWYRFDSCfL6Mx_4OWwUrziLVXw"

export const configHeaders = () => {
    return {
        TokenCybersoft: TokenCybersoft,
        token: token
    };
};

export const https = axios.create({
    baseURL: BASE_URL,
    headers: configHeaders(),
})