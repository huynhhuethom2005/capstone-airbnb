export const localServ = {
  get: () => {
    let dataUser = localStorage.getItem("USER_INFO");
    return dataUser
  },
  set: (userInfo) => {
    let dataUser = JSON.stringify(userInfo);
    localStorage.setItem("USER_INFO", dataUser);
  },
  remove: () => {
    localStorage.removeItem("USER_INFOR");
  },
};
