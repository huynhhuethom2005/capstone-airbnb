import { https } from "./config"
import { localServ } from "./localService"

export const userServ = {
    postLogin: (dataUser) => {
        return https.post(`/api/auth/signin`, dataUser)
    },
    postSignup: (dataUserRegis) => {
        return https.post(`/api/auth/signup`, dataUserRegis)
    },
    getPersonalInfo: (idUser) => {
        return https.get(`/api/users/${idUser}`)
    },
    checkUserLogin: () => {
        return localServ.get() ? true : false
    }
}