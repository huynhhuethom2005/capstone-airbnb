import { https } from "./config"

export const adminUserServ = {
    getAllUser: () => {
        return https.get(`/api/users`)
    },
    deleteUser: (id) => {
        return https.delete(`/api/users?id=${id}`)
    },
    createUser: (dataUser) => {
        return https.post(`/api/users`, dataUser)
    }
}