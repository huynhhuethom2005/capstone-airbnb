import { https } from "./config";

export const phongService = {
  getPhongPhanTrangTimKiem: (pageIndex, pageSize) => {
    return https.get(
      `/api/phong-thue/phan-trang-tim-kiem?pageIndex=${pageIndex}&pageSize=${pageSize}`
    );
  },

  getPhongThue: () => {
    return https.get('/api/phong-thue')
  },

  getPhongTheoViTri: (id) => {
    return https.get(`/api/phong-thue/lay-phong-theo-vi-tri?maViTri=${id}`);
  },

  getPhongThueId: (id) => {
    return https.get(`/api/phong-thue/${id}`);
  },

  deletePhong: (id) => {
    return https.delete(`/api/phong-thue/${id}`)
  },

  postPhong: (data) => {
    return https.post('/api/phong-thue', data)
  },

  putPhong: (data) => {
    return https.put(`/api/phong-thue/${data.id}`, data.values)
  }
}
