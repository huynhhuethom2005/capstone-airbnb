import { https } from "./config"

export const binhluanService = {
    getBinhLuanTheoPhong: (id) => {
        return https.get(`/api/binh-luan/lay-binh-luan-theo-phong/${id}`)
    }
}