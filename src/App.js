import logo from './logo.svg';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import { userRoutes } from './routers/userRoute';
import { adminRoutes } from './routers/adminRoutes';
import { publicRoutes } from './routers/publicRiutes';

function App() {
  const hanldeRenderRoutes = (routeServ) => {
    return routeServ.map(({ url, component }) => {
      return <Route path={url} element={component} />
    })
  }
  return (
    <div className="min-h-screen">
      <BrowserRouter>
        <Routes>
          {hanldeRenderRoutes(userRoutes)}
          {hanldeRenderRoutes(adminRoutes)}
          {hanldeRenderRoutes(publicRoutes)}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
