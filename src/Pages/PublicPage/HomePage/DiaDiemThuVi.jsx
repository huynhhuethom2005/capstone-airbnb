import React, { useEffect, useState } from 'react'
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

import "./styles.css";

// import required modules
import { EffectCoverflow } from "swiper";
import { vitriService } from '../../../service/vitriService';

import { Image } from 'antd';

export default function DiaDiemThuVi() {
    const [vitri, setVitri] = useState([])

    useEffect(() => {
        vitriService.getVitri().then((result) => {
            setVitri(result.data.content)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }, [])

    return (
        <>
            <Swiper
                effect={"coverflow"}
                grabCursor={true}
                centeredSlides={true}
                slidesPerView={"auto"}
                coverflowEffect={{
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                }}
                // pagination={true}
                modules={[EffectCoverflow]}
                className="mySwiper"
            >
                {vitri.map(item => {
                    return <SwiperSlide>
                        <Image style={{ width: "100%", height: "160px", "object-fit": "cover" }} src={item.hinhAnh} />
                        <div className='text-center font-medium font-sans text-lg'>{item.tenViTri}, {item.tinhThanh}, {item.quocGia}</div>
                    </SwiperSlide>
                })}
            </Swiper>
        </>
    )
}
