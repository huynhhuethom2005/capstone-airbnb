import React from 'react'

import { EditOutlined } from '@ant-design/icons';
import { Card } from 'antd';
import { Image } from 'antd';
import { NavLink } from 'react-router-dom';

const { Meta } = Card;

export default function ThongTinPhong({ item }) {

    return (
        <>
            <Card
                style={{
                    width: "100%",
                }}
                cover={
                    <Image style={{ width: "100%", height: "160px", "object-fit": "cover" }} src={item.hinhAnh} />
                }
                actions={[
                    <NavLink to={`/detail/${item.id}`}> <EditOutlined key="edit" /> </NavLink>,
                ]}
            >
                <Meta
                    title={<NavLink to={`/detail/${item.id}`}>{item.tenPhong}</NavLink>}
                    description={<NavLink to={`/detail/${item.id}`}>Số khách: {item.phongNgu} - ${item.giaTien}/đêm</NavLink>}
                />
            </Card>
        </>
    )
}
