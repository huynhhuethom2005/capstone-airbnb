import React, { useEffect, useState } from 'react'

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";

import "./styles.css";

import { Pagination } from 'antd';
import { phongService } from '../../../service/phongService';
import { FloatButton } from 'antd';
import DiaDiemThuVi from './DiaDiemThuVi';
import ThongTinPhong from './ThongTinPhong';

export default function HomePage() {
  const [phong, setPhong] = useState([])
  const [pageIndex, setPageIndex] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [totalPages, setTotalPages] = useState(7)

  const handleSetPage = (pageIndex, pageSize) => {
    setPageIndex(pageIndex)
    setPageSize(pageSize)
  }

  useEffect(() => {
    phongService.getPhongPhanTrangTimKiem(pageIndex, pageSize).then((result) => {
      const data = result.data.content
      setPhong(data.data)
      setTotalPages(Math.ceil(data.totalRow / data.pageSize) * 10)
    }).catch((err) => {
      console.log('err :>> ', err);
    });
  }, [pageIndex, pageSize])

  return (
    <div className='mx-0 md:mx-20'>
      <h1 className='text-center mt-10 font-medium font-sans text-2xl'>Khám phá những địa điểm thú vị</h1>
      <DiaDiemThuVi />

      <div className="m-10">
        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-10">
          {phong.map(item => {
            return <ThongTinPhong item={item} />
          })}
        </div>
      </div>

      <div className="m-10">
        <Pagination defaultCurrent={pageIndex} total={totalPages} onChange={(page, pageSize) => { handleSetPage(page, pageSize) }} />
      </div>
      <FloatButton.BackTop />
    </div>
  )
}
