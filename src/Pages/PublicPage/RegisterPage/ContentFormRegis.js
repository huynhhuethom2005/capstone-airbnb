import React from 'react'
import { Button, Checkbox, DatePicker, Form, Input, Select, message } from 'antd';
import { Desktop, Mobile, Tablet } from '../../../Layout/Reponsive';

export default function ContentFormRegis() {
    return (
        <div>
            <Desktop>
                <div className='grid grid-cols-2 gap-2'>
                    <Form.Item
                        label="Tên người dùng"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Tên người dùng không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Điền tên vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Email không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Example@gmail.com' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Số điện thoại"
                        name="phone"
                    >
                        <Input placeholder='098 6888 234' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Mật khẩu"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Mật khẩu không được để trống!',
                            },
                        ]}
                    >
                        <Input.Password placeholder='*********' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Địa chỉ"
                        name="address"
                    >
                        <Input placeholder='Điền địa chỉ vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Ngày sinh"
                        name="birthday"
                    >
                        <DatePicker placeholder='mm/dd/yyyy' className='p-2 w-full' />
                    </Form.Item>
                    <Form.Item
                        label="Giới tính"
                        name="gender"
                    >
                        <Select placeholder='----Chọn giới tính----'>
                            <Select.Option value="true">Nam</Select.Option>
                            <Select.Option value="false">Nữ</Select.Option>
                        </Select>

                    </Form.Item>
                </div>
            </Desktop>
            <Tablet>
                <div>
                    <Form.Item
                        label="Tên người dùng"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Tên người dùng không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Điền tên vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Email không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Example@gmail.com' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Số điện thoại"
                        name="phone"
                    >
                        <Input placeholder='098 6888 234' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Mật khẩu"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Mật khẩu không được để trống!',
                            },
                        ]}
                    >
                        <Input.Password placeholder='*********' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Địa chỉ"
                        name="address"
                    >
                        <Input placeholder='Điền địa chỉ vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Ngày sinh"
                        name="birthday"
                    >
                        <DatePicker placeholder='mm/dd/yyyy' className='p-2 w-full' />
                    </Form.Item>
                    <Form.Item
                        label="Giới tính"
                        name="gender"
                    >
                        <Select placeholder='----Chọn giới tính----'>
                            <Select.Option value="true">Nam</Select.Option>
                            <Select.Option value="false">Nữ</Select.Option>
                        </Select>

                    </Form.Item>
                </div>
            </Tablet>
            <Mobile>
                <div>
                    <Form.Item
                        label="Tên người dùng"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Tên người dùng không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Điền tên vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Email không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Example@gmail.com' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Số điện thoại"
                        name="phone"
                    >
                        <Input placeholder='098 6888 234' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Mật khẩu"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Mật khẩu không được để trống!',
                            },
                        ]}
                    >
                        <Input.Password placeholder='*********' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Địa chỉ"
                        name="address"
                    >
                        <Input placeholder='Điền địa chỉ vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Ngày sinh"
                        name="birthday"
                    >
                        <DatePicker placeholder='mm/dd/yyyy' className='p-2 w-full' />
                    </Form.Item>
                    <Form.Item
                        label="Giới tính"
                        name="gender"
                    >
                        <Select placeholder='----Chọn giới tính----'>
                            <Select.Option value="true">Nam</Select.Option>
                            <Select.Option value="false">Nữ</Select.Option>
                        </Select>

                    </Form.Item>
                </div>
            </Mobile>
        </div>
    )
}
