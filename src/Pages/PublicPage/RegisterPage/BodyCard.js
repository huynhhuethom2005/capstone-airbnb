import React from 'react'
import { Button, Checkbox, DatePicker, Form, Input, Select, message } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { userServ } from '../../../service/userService';
import { localServ } from '../../../service/localService';
import { setUserLogin } from '../../../toolkit/userSlice';
import ContentFormRegis from './ContentFormRegis';

export default function BodyCard() {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const onFinish = (values) => {
        userServ.postSignup(values)
            .then(response => {
                message.success('Đăng ký thành công')

                navigate('/login')
            })
            .catch(error => {
                console.log("error :", error)
                message.error('Đăng ký không thành công')
            })
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className='w-full'>
            <Form
                name="basic"
                style={{
                    width: '100%',
                    padding: '10px',
                }}
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <ContentFormRegis />


                <Form.Item>
                    <div className='flex justify-around items-center '>
                        <Button className='w-1/2 h-10 rounded-full' type="primary" danger htmlType="submit">Đăng ký</Button>
                    </div>
                </Form.Item>
                <Form.Item>
                    <div className='flex justify-center '>
                        <NavLink to={'/login'}>
                            <a href='#' className='text-lg text-red-500 hover:text-red-500 hover:underline hover:decoration-red-500'> Đăng nhập ngay</a>
                        </NavLink>
                    </div>
                </Form.Item>
            </Form>
        </div>
    )
}
