import React, { useState } from 'react'
import { Button, Card, Form, Modal, Upload } from 'antd';
import tickSuccess from '../../../asset/tick-success.png'
import { EditOutlined, EllipsisOutlined, SettingOutlined, PlusOutlined } from '@ant-design/icons';

export default function CardContent() {
    const { Meta } = Card;
    const [isModalOpen, setIsModalOpen] = useState(false);
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };
    return (
        <div>
            <Card
                className='w-full'

                cover={<div className='p-4'>
                    <img
                        className='w-1/3 container mx-auto rounded-full'
                        // style={{ width: '50%', height: '20%' }}
                        alt="example"
                        src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
                    />
                    <div className='text-center'>
                        <a className='text-base underline' onClick={showModal}>Cập nhật ảnh</a>
                    </div>
                    <Modal title="Cập nhật ảnh đại diện" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                        <Form>
                            <Form.Item>
                                <Upload className='text-center' listType="picture-card">
                                    <div>
                                        <PlusOutlined />
                                        <div
                                            style={{
                                                marginTop: 8,
                                            }}
                                        >
                                            Upload
                                        </div>
                                    </div>
                                </Upload>
                            </Form.Item>
                        </Form>
                    </Modal>
                    <div className='flex items-center '>
                        <img
                            style={{
                                width: '10%',
                            }}
                            src={tickSuccess} />

                        <h1 className='text-2xl font-bold'>Xác minh danh tính</h1>
                    </div>
                    <span className='text-base'>Xác minh danh tính của bạn với huy hiệu xác minh danh tính.</span>
                    <button className='border-[1px] p-2 w-1/3 rounded-lg mt-4 font-bold hover:bg-slate-200 mb-4'>Nhận huy hiệu</button>
                    <hr></hr>
                    <div>
                        <h1 className='font-bold text-2xl mt-4 mb-4'>Đã xác nhận</h1>
                        <div className='flex items-center'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12.75l6 6 9-13.5" />
                            </svg>
                            <span>Địa chỉ email</span>
                        </div>
                    </div>
                </div>}

            >

            </Card>
        </div>
    )
}
