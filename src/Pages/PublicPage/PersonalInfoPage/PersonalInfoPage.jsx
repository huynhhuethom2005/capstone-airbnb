import React, { useEffect, useState } from 'react'
import { userServ } from '../../../service/userService'
import { useParams } from 'react-router'

import { Button, Checkbox, Form, Input, Space } from 'antd';
import CardContent from './CardContent';
import FormContent from './FormContent';



export default function PersonalInfoPage() {
  const [userInfo, setUserInfo] = useState([])
  const { id } = useParams()
  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  useEffect(() => {
    userServ.getPersonalInfo(id)
      .then(res => {
        setUserInfo(res.data.content)
      })
      .catch(err => {
        console.log(err)
      })
  }, [])


  return (
    <div className='container grid grid-cols-3 justify-center items-center mx-auto py-8'>
      <CardContent />
      <FormContent userInfo={userInfo} />
    </div>
  )
}
