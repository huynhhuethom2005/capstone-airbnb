import React, { useState } from 'react'
import { Button, Checkbox, Form, Input, Space, message } from 'antd';
import CardContent from './CardContent';

export default function FormContent(props) {

    const [inPutInfo, setInPutInfo] = useState({
        name: true,
        gender: true,
        birthday: true,
        email: true,
        phone: true,
    });

    const setInputInfo = (key) => {
        switch (key) {
            case 'name':
                setInPutInfo({ ...inPutInfo, name: !inPutInfo.name });
                break;
            case 'gender':
                setInPutInfo({ ...inPutInfo, gender: !inPutInfo.gender });
                break;
            case 'birthday':
                setInPutInfo({ ...inPutInfo, birthday: !inPutInfo.birthday });
                break;
            case 'email':
                setInPutInfo({ ...inPutInfo, email: !inPutInfo.email });
                break;
            case 'phone':
                setInPutInfo({ ...inPutInfo, phone: !inPutInfo.phone });
                break;
        }
    }
    let { name, email, phone, birthday, gender } = props.userInfo
    return (
        <div className='ml-4'>
            <Form>
                <div>
                    <div className='border-b py-5'>
                        <div className='flex justify-around items-center'>
                            <div>
                                <p className='text-2xl'>Tên pháp lý</p>
                                <p>{name}</p>
                            </div>
                            <div>
                                <a className='underline' onClick={() => {
                                    setInputInfo('name')
                                }}>Thay đổi</a>
                            </div>
                        </div>
                    </div>
                    <div className={`mt-2 ${inPutInfo.name ? 'hidden' : 'block'}`} >
                        <Space.Compact style={{ width: '100%' }}>
                            <Input defaultValue={name} />
                            <Button type="primary" onClick={() => {
                                setInputInfo('name')
                                message.error('Cập nhật thất bại')
                            }} danger>Lưu</Button>
                        </Space.Compact>
                    </div>
                </div>
                <div>
                    <div className='border-b py-5'>
                        <div className='flex justify-around items-center'>
                            <div>
                                <p className='text-2xl'>Giới tính</p>
                                <p>{gender ? 'Nam' : 'Nữ'}</p>
                            </div>
                            <div>
                                <a className='underline' onClick={() => {
                                    setInputInfo('gender')
                                }}>Thay đổi</a>
                            </div>
                        </div>
                    </div>
                    <div className={`mt-2 ${inPutInfo.gender ? 'hidden' : 'block'}`} >
                        <Space.Compact style={{ width: '100%' }}>
                            <Input defaultValue={gender} />
                            <Button type="primary" onClick={() => {
                                setInputInfo('gender')
                                message.error('Cập nhật thất bại')
                            }} danger>Lưu</Button>
                        </Space.Compact>
                    </div>
                </div>
                <div>
                    <div className='border-b py-5'>
                        <div className='flex justify-around items-center'>
                            <div>
                                <p className='text-2xl'>Ngày sinh</p>
                                <p>{birthday}</p>
                            </div>
                            <div>
                                <a className='underline' onClick={() => {
                                    setInputInfo('birthday')
                                }}>Thay đổi</a>
                            </div>
                        </div>
                    </div>
                    <div className={`mt-2 ${inPutInfo.birthday ? 'hidden' : 'block'}`} >
                        <Space.Compact style={{ width: '100%' }}>
                            <Input defaultValue={birthday} />
                            <Button type="primary" onClick={() => {
                                setInputInfo('birthday')
                                message.error('Cập nhật thất bại')
                            }} danger>Lưu</Button>
                        </Space.Compact>
                    </div>
                </div>
                <div>
                    <div className='border-b py-5'>
                        <div className='flex justify-around items-center'>
                            <div>
                                <p className='text-2xl'>Địa chỉ email</p>
                                <p>{email}</p>
                            </div>
                            <div>
                                <a className='underline' onClick={() => {
                                    setInputInfo('email')
                                }}>Thay đổi</a>
                            </div>
                        </div>
                    </div>
                    <div className={`mt-2 ${inPutInfo.email ? 'hidden' : 'block'}`} >
                        <Space.Compact style={{ width: '100%' }}>
                            <Input defaultValue={email} />
                            <Button type="primary " onClick={() => {
                                setInputInfo('email')
                                message.error('Cập nhật thất bại')
                            }} danger>Lưu</Button>
                        </Space.Compact>
                    </div>
                </div>
                <div>
                    <div className='border-b py-5'>
                        <div className='flex justify-around items-center'>
                            <div>
                                <p className='text-2xl'>Số điện thoại</p>
                                <p>{phone}</p>
                            </div>
                            <div>
                                <a className='underline' onClick={() => {
                                    setInputInfo('phone')
                                }}>Thay đổi</a>
                            </div>
                        </div>
                    </div>
                    <div className={`mt-2 ${inPutInfo.phone ? 'hidden' : 'block'}`} >
                        <Space.Compact style={{ width: '100%' }}>
                            <Input defaultValue={phone} />
                            <Button type="primary" onClick={() => {
                                setInputInfo('phone')
                                message.error('Cập nhật thất bại')
                            }} danger>Lưu</Button>
                        </Space.Compact>
                    </div>
                </div>
            </Form>
        </div>
    )
}
