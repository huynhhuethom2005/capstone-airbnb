import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { vitriService } from "../../../service/vitriService";
import { phongService } from "../../../service/phongService";
import RoomList from "./RoomList";

export default function RoomByCityPage() {
  let { id } = useParams();
  const [rooms, setRooms] = useState([]);

  useEffect(() => {
    phongService
      .getPhongTheoViTri(id)
      .then((res) => {
        console.log(res.data.content);
        setRooms(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="mt-20 grid grid-cols-1 md:grid-cols-2 gap-6">
      <div>
        <div className="flex flex-wrap justify-between items-center py-5 ml-4">
          <span className="font-semibold">Hơn 1.000 chổ ở</span>
          <button
            type="button"
            className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 flex flex-wrap justify-center items-center"
          >
            <svg
              viewBox="0 0 16 16"
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              role="presentation"
              focusable="false"
              style={{
                display: "block",
                height: 16,
                width: 16,
                fill: "currentcolor",
              }}
            >
              <path d="M5 8c1.306 0 2.418.835 2.83 2H14v2H7.829A3.001 3.001 0 1 1 5 8zm0 2a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm6-8a3 3 0 1 1-2.829 4H2V4h6.17A3.001 3.001 0 0 1 11 2zm0 2a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
            </svg>
            <span>Bộ lọc</span>
          </button>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 gap-6 ml-4">
          {rooms.map((item) => {
            return <RoomList data={item} key={item.id} />;
          })}
        </div>
      </div>

      <div className="h-screen w-full hidden md:block sticky top-28">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7862661.399813475!2d105.91025889999999!3d15.793925249999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31157a4d736a1e5f%3A0xb03bb0c9e2fe62be!2zVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1684568392807!5m2!1svi!2s"
          className="w-full h-full"
          style={{ border: 0 }}
          allowFullScreen
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        />
      </div>
    </div>
  );
}
