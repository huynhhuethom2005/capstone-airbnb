import React from "react";
import { Card, Typography } from "antd";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

const { Meta } = Card;
const { Paragraph } = Typography;

export default function RoomList({ data }) {
  return (
    <Card
      hoverable
      style={{
        width: 280,
        height: 480,
      }}
      cover={
        <img
          alt="example"
          src={data.hinhAnh}
          className=" w-56 h-80 object-cover object-left"
        />
      }
    >
      <NavLink
        className={
          "text-gray-500 text-ellipsis overflow-hidden whitespace-nowrap"
        }
        to={`/detail/${data.id}`}
      >
        <Meta
          description={
            <>
              <button className="absolute top-3 right-3 z-30">
                <svg
                  viewBox="0 0 32 32"
                  xmlns="http://www.w3.org/2000/svg"
                  aria-hidden="true"
                  role="presentation"
                  focusable="false"
                  style={{
                    display: "block",
                    fill: "rgba(0, 0, 0, 0.5)",
                    height: 24,
                    width: 24,
                    stroke: "rgb(255, 255, 255)",
                    strokeWidth: 2,
                    overflow: "hidden",
                  }}
                >
                  <path d="m16 28c7-4.733 14-10 14-17 0-1.792-.683-3.583-2.05-4.95-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05l-2.051 2.051-2.05-2.051c-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05-1.367 1.367-2.051 3.158-2.051 4.95 0 7 7 12.267 14 17z" />
                </svg>
              </button>

              <div className="flex justify-between mt-2">
                <span className=" font-black text-ellipsis overflow-hidden whitespace-nowrap">
                  {data.tenPhong}
                </span>
                <span>
                  <FontAwesomeIcon icon={faStar} />
                  {(Math.random() * 4 + 1).toFixed(2)}
                </span>
              </div>

              <Paragraph ellipsis={{ rows: 2 }}>{data.moTa}</Paragraph>

              <strong>{data.giaTien}0000 VNĐ / đêm</strong>
            </>
          }
        />
      </NavLink>
    </Card>
  );
}
