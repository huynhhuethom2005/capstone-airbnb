import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { phongService } from '../../../service/phongService'
import { Image } from 'antd';
import { InstagramOutlined, StarOutlined, TwitterOutlined, WechatOutlined, YoutubeOutlined } from '@ant-design/icons';
import ViTri from './ViTri';
import BinhLuan from './BinhLuan';
import TienIch from './TienIch';
import DatPhong from './DatPhong';

export default function DetailPage() {
  const { id } = useParams()
  const [detailInfo, setDetailInfo] = useState()

  useEffect(() => {
    phongService.getPhongThueId(id).then((result) => {
      setDetailInfo(result.data)
    }).catch((err) => {
      console.log('err :>> ', err);
    });
  }, [])
  
  return (
    <div className='mx-0 md:mx-20 font-sans'>
      <h1 className='text-2xl font-bold my-5'>{detailInfo?.content.tenPhong}</h1>
      <div className='flex justify-between items-center my-3'>
        <div className="flex items-end space-x-2">
          <span>5 </span>
          <StarOutlined className='text-lg' />
          <ViTri id={id} />
        </div>
        <div className='text-lg space-x-2' >
          <TwitterOutlined />
          <YoutubeOutlined />
          <WechatOutlined />
          <InstagramOutlined />
        </div>
      </div>
      <Image style={{ width: "100%", "object-fit": "cover", "border-radius": "20px" }} src={detailInfo?.content.hinhAnh} />
      <div className='my-5 grid grid-cols-1 md:grid-cols-5'>
        <div className='col-span-3'>
          <h1 className='text-2xl font-bold'>Toàn bộ căn hộ. Chủ nhà Sungwon</h1>
          <div className='mb-5'>Khách {detailInfo?.content.khach} . {detailInfo?.content.phongNgu} Phòng ngủ . {detailInfo?.content.phongTam} Phòng tắm</div>
          <hr />
          <div className='my-5'>{detailInfo?.content.moTa}</div>
          <hr />
          <h1 className='text-2xl font-bold my-5'>Nơi này có những gì cho bạn</h1>
          <TienIch detailInfo={detailInfo} />
        </div>
        <div className='col-span-2'>
          <DatPhong giaPhong={detailInfo?.content.giaTien} maPhong={detailInfo?.content.id} />
        </div>
      </div>
      <hr />
      <BinhLuan id={id} />
      <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    </div>
  )
}
