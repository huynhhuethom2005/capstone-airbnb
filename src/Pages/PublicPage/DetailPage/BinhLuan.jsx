import React, { useEffect, useState } from 'react'
import { binhluanService } from '../../../service/binhluanService';
import { Avatar, Card } from 'antd';
import Meta from 'antd/es/card/Meta';
import moment from 'moment';

export default function BinhLuan({ id }) {
    const [binhLuan, setBinhLuan] = useState([])

    useEffect(() => {
        binhluanService.getBinhLuanTheoPhong(id).then((result) => {
            setBinhLuan(result.data.content)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }, [])
    return (
        <>
            <div>
                <h1 className='text-2xl font-bold my-5'>Đánh giá</h1>
                <div className='grid grid-cols-1 md:grid-cols-2'>
                    {binhLuan.map(item => {
                        return <Card
                            style={{
                                width: 600,
                                border: "none"
                            }}
                        >
                            <Meta
                                avatar={<Avatar src={item.avatar} />}
                                title={`${item.tenNguoiBinhLuan} - ${moment(item.ngayBinhLuan).format('DD/MM/YYYY, h:mm:ss a')}`}
                                description={item.noiDung}
                            />
                        </Card>
                    })}

                </div>
            </div>
        </>
    )
}
