import React, { useEffect, useState } from 'react'
import { vitriService } from '../../../service/vitriService';

export default function ViTri({ id }) {
    const [vitri, setVitri] = useState()

    useEffect(() => {
        vitriService.getViTriId(id).then((result) => {
            setVitri(result.data.content)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }, [])
    return (
        <>
            <span>{vitri?.tenViTri}, {vitri?.tinhThanh}, {vitri?.quocGia}</span>
        </>
    )
}
