import React, { useState } from 'react'
import { DatePicker, Space, InputNumber, message } from 'antd';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { datPhongService } from '../../../service/datphongService';
import { userServ } from '../../../service/userService';

dayjs.extend(customParseFormat);
const { RangePicker } = DatePicker;
const dateFormat = 'YYYY-MM-DD';

export default function DatPhong({ giaPhong, maPhong }) {
    const nowDate = new Date()
    const nowstring = nowDate.toISOString()

    const [ngayDen, setNgayDen] = useState(nowstring)
    const [ngayDi, setNgayDi] = useState(nowstring)
    const [soLuongKhach, setSoLuongKhach] = useState(1)
    const [maNguoiDung, setMaNguoiDung] = useState(1)

    const thongTinDatPhong = { maPhong, ngayDi, ngayDen, soLuongKhach, maNguoiDung }

    const handleSetTime = (dateString) => {
        setNgayDen(dateString[0])
        setNgayDi(dateString[1])
    }

    const handleSetSoLuongKhach = (value) => {
        setSoLuongKhach(value)
    };

    const handleDatPhong = () => {
        if (userServ.checkUserLogin()) {
            datPhongService.postDatPhong(thongTinDatPhong).then((result) => {
                message.success("Đặt phòng thành công")
            }).catch((err) => {
                message.error("Đặt phòng thất bại")
            });
        } else {
            message.error("Vui lòng đăng nhập để đặt phòng!")
        }
    }

    return (
        <div className='mx-10 border rounded p-5'>
            <div className='text-xl font-bold mb-5'>
                $ {giaPhong}/ngày
            </div>
            <hr />

            <div className='text-xl font-bold my-5'>Nhận phòng - Trả phòng</div>
            <Space
                direction="vertical"
                style={{
                    width: '100%',
                }}
            >
                <RangePicker
                    status="warning"
                    style={{
                        width: '100%',
                    }}
                    defaultValue={[dayjs(nowstring, dateFormat), dayjs(nowstring, dateFormat)]}
                    onChange={(_, dateString) => { handleSetTime(dateString) }}
                />
            </Space>
            <hr className='mt-5' />

            <div className='text-xl font-bold my-5'>Số lượng khách</div>
            <Space>
                <InputNumber min={1} max={100000} defaultValue={1} onChange={(value) => { handleSetSoLuongKhach(value) }} />
            </Space>

            <button type="button" className='w-full my-5 border rounded py-3 text-xl text-white font-bold bg-pink-700'
                onClick={handleDatPhong}
            >
                Đặt phòng
            </button>
        </div>
    )
}
