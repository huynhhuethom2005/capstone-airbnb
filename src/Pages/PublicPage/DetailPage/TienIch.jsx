import { BorderOutlined, CarOutlined, CompassOutlined, DesktopOutlined, InstagramOutlined, NodeIndexOutlined, SkinOutlined, SmileOutlined, StarOutlined, SubnodeOutlined, TwitterOutlined, UngroupOutlined, WechatOutlined, WifiOutlined, YoutubeOutlined } from '@ant-design/icons';
import React from 'react'

export default function TienIch({ detailInfo }) {
    return (
        <>
            <div className='grid grid-cols-2 gap-5'>
                {detailInfo?.content.mayGiat ? <div> <SkinOutlined /> Máy giặt</div> : <></>}
                {detailInfo?.content.laBan ? <div> <CompassOutlined /> La bàn</div> : <></>}
                {detailInfo?.content.tiVi ? <div> <DesktopOutlined /> Tivi</div> : <></>}
                {detailInfo?.content.dieuHoa ? <div> <UngroupOutlined /> Điều hoà</div> : <></>}
                {detailInfo?.content.wifi ? <div> <WifiOutlined /> Wifi</div> : <></>}
                {detailInfo?.content.bep ? <div> <BorderOutlined /> Bếp</div> : <></>}
                {detailInfo?.content.doXe ? <div> <CarOutlined /> Đỗ xe</div> : <></>}
                {detailInfo?.content.hoBoi ? <div> <SmileOutlined /> Đỗ xe</div> : <></>}
                {detailInfo?.content.banUi ? <div> <SubnodeOutlined /> Đỗ xe</div> : <></>}
            </div>
        </>
    )
}
