import React from 'react'
import { Button, Checkbox, Form, Input, message } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { userServ } from '../../../service/userService';
import { localServ } from '../../../service/localService';
import { setUserLogin } from '../../../toolkit/userSlice';

export default function BodyCard() {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const onFinish = (values) => {
        userServ.postLogin(values)
            .then(response => {
                message.success('Đăng nhập thành công')
                
                dispatch(setUserLogin(response))
                navigate('/')
            })
            .catch(error => {
                console.log("error :", error)
                message.error('Đăng nhập không thành công')
            })
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className='w-full'>
            <Form
                name="basic"
                style={{
                    width: '100%',
                    padding: '10px',
                }}
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                >
                    <Input  className='p-2'/>
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="password"
                >
                    <Input.Password className='p-2' />
                </Form.Item>


                <Form.Item>
                    <div className='flex justify-around items-center '>
                        <a href='#' className='hover:text-red-500 '>Quên mật khẩu ?</a>
                        <Button className='w-1/2 h-10' type="primary" danger htmlType="submit">Đăng nhập</Button>
                    </div>
                </Form.Item>
                <Form.Item>
                    <div className='flex justify-center '>
                        Chưa có tài khoản? <NavLink to={'/register'}>
                            <a href='#' className='hover:text-red-500'> Đăng ký ngay</a>
                        </NavLink>
                    </div>
                </Form.Item>
            </Form>
        </div>
    )
}
