import React from 'react'
import bg_login from '../../../asset/bg-login.jpg'
import { Card } from 'antd'
import HeaderCard from './HeaderCard';
import BodyCard from './BodyCard';
import { Desktop, Mobile, Tablet } from '../../../Layout/Reponsive';

export default function LoginPage() {
  return (
    <div style={{ backgroundImage: `url(${bg_login})` }}
      className='w-full h-screen bg-no-repeat bg-cover flex justify-center items-center p-5'>
      <Desktop>
        <Card title={<HeaderCard />} className='w-1/2 '>
          <BodyCard />
        </Card>
      </Desktop>
      <Tablet>
        <Card title={<HeaderCard />} className='w-full'>
          <BodyCard />
        </Card>
      </Tablet>
      <Mobile>
        <Card title={<HeaderCard />} className='w-full '>
          <BodyCard />
        </Card>
      </Mobile>
    </div>
  )
}
