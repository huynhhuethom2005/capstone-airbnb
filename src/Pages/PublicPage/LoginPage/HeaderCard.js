import React from 'react'
import logoAirBNB from '../../../asset/logoAirBNB.png'
import { Desktop, Mobile, Tablet } from '../../../Layout/Reponsive'
export default function HeaderCard() {
  return (
    <div>
      <Desktop>
        <div className='grid grid-cols-2 p-2 items-center'>
          <img src={logoAirBNB} className='w-1/3 ' />
          <h2 className='text-4xl font-semibold text-blue-800 '>Đăng nhập</h2>
        </div>
      </Desktop>
      <Tablet>
        <div className='flex items-center justify-center'>
          <img src={logoAirBNB} className='w-1/3' />
        </div>
      </Tablet>
      <Mobile>
        <div className='flex items-center justify-center'>
          <img src={logoAirBNB} className='w-1/3' />
        </div>
      </Mobile>
    </div>
  )
}
