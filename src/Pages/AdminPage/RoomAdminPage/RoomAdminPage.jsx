import React, { useEffect, useState } from 'react'
import { Space, Table, Modal, message } from 'antd';
import { phongService } from '../../../service/phongService';
import { columns } from './utils';
import Spinner from '../../../Components/Spinner/Spinner';
import { NavLink, useNavigate } from 'react-router-dom';
import { setFormValue } from '../../../toolkit/adminPhongSlice';
import { useDispatch } from 'react-redux';

export default function RoomAdminPage() {
    const [phong, setPhong] = useState([])
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
    const [idDelete, setIdDelete] = useState();
    const [isLoading, setIsLoading] = useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const showModalDelete = (id) => {
        setIdDelete(id)
        setIsModalDeleteOpen(true);
    };

    const fetchPhong = () => {
        setIsLoading(true)
        phongService.getPhongThue().then((result) => {
            setPhong(result.data.content)
            setIsLoading(false)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }

    const handleOkDelete = () => {
        phongService.deletePhong(idDelete).then((result) => {
            fetchPhong()
            message.success("Xoá thành công!")
        }).catch((err) => {
            console.log('err :>> ', err);
        });
        setIsModalDeleteOpen(false);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
    };

    const handleAddnew = () => {
        dispatch(setFormValue(null))
        navigate('/admin/quanlyphong/newroom')
    }

    const handleSetValueUpdate = (id) => {
        phongService.getPhongThueId(id).then((result) => {
            dispatch(setFormValue(result.data.content))
            navigate(`/admin/quanlyphong/update/${id}`)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }

    const actionBtn = [
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: 150,
            render: (_, record) => (
                <Space size="middle">
                    <button onClick={() => { showModalDelete(record.id) }} type="button" className='rounded bg-red-500 p-2 text-white hover:bg-red-200 transition'>Xoá</button>
                    <button
                        onClick={() => { handleSetValueUpdate(record.id) }}
                        type="button" className='rounded bg-yellow-400 p-2 text-white hover:bg-yellow-200 transition'>Sửa</button>
                </Space >
            ),
        }
    ]

    const columnsClone = [...columns, ...actionBtn]

    useEffect(() => {
        fetchPhong()
    }, [])

    return (
        <div>
            <Spinner isLoading={isLoading} />

            {/* <NavLink to="/admin/quanlyphong/newroom"> */}
            <button
                onClick={handleAddnew}
                type="button" className='p-3 my-3 bg-blue-600 rounded text-white font-bold hover:bg-blue-400'>Thêm phòng mới</button>
            {/* </NavLink> */}
            <Table columns={columnsClone} dataSource={phong} scroll={{ x: 1000, y: 650 }} />
            {/* Modal Delete */}
            <Modal title="Xoá dữ liệu" open={isModalDeleteOpen} onOk={handleOkDelete} onCancel={handleCancelDelete}>
                <p>Bạn có chắc muốn xoá phòng này khum!!!</p>
            </Modal>
        </div>
    )
}
