import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Space, Table, Tag } from 'antd';
import { Image } from 'antd';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        width: 100,
        fixed: 'left',
    },
    {
        title: 'Tên phòng',
        dataIndex: 'tenPhong',
        key: 'tenPhong',
        width: 200,
        fixed: 'left',
    },
    {
        title: 'Khách',
        dataIndex: 'khach',
        key: 'khach',
        width: 100,
    },
    {
        title: 'Phòng ngủ',
        dataIndex: 'phongNgu',
        key: 'phongNgu',
        width: 100,
    },
    {
        title: 'Giường',
        dataIndex: 'giuong',
        key: 'giuong',
        width: 100,
    },
    {
        title: 'Phòng tắm',
        dataIndex: 'phongTam',
        key: 'phongTam',
        width: 100,
    },
    {
        title: 'Mô tả',
        dataIndex: 'moTa',
        key: 'moTa',
        width: 500,
        render: (moTa, _) => (
            <div> {moTa}</div >
        )
    },
    {
        title: 'Giá tiền',
        dataIndex: 'giaTien',
        key: 'giaTien',
        width: 100,
    },
    {
        title: 'Máy giặt',
        dataIndex: 'mayGiat',
        key: 'mayGiat',
        width: 100,
        render: (mayGiat, _) => (<div>{mayGiat ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'La bàn',
        dataIndex: 'banLa',
        key: 'banLa',
        width: 100,
        render: (banLa, _) => (<div>{banLa ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Tivi',
        dataIndex: 'tivi',
        key: 'tivi',
        width: 100,
        render: (tivi, _) => (<div>{tivi ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Điều hoà',
        dataIndex: 'dieuHoa',
        key: 'dieuHoa',
        width: 100,
        render: (dieuHoa, _) => (<div>{dieuHoa ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Wifi',
        dataIndex: 'wifi',
        key: 'wifi',
        width: 100,
        render: (wifi, _) => (<div>{wifi ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Bếp',
        dataIndex: 'bep',
        key: 'bep',
        width: 100,
        render: (bep, _) => (<div>{bep ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Đỗ xe',
        dataIndex: 'doXe',
        key: 'doXe',
        width: 100,
        render: (doXe, _) => (<div>{doXe ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Bàn ủi',
        dataIndex: 'banUi',
        key: 'banUi',
        width: 100,
        render: (banUi, _) => (<div>{banUi ? <Tag color='green' >
            <CheckOutlined />
        </Tag> : <Tag color='volcano' >
            <CloseOutlined />
        </Tag>}</div>)
    },
    {
        title: 'Hình ảnh',
        dataIndex: 'hinhAnh',
        key: 'hinhAnh',
        width: 100,
        render: (hinhAnh, _) => (<div><Image
            width={'100%'}
            src={hinhAnh}
        /></div>)
    },
    
];

export { columns }