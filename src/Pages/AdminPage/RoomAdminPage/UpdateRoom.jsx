import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import FormInforPhong from './FormInforPhong'
import { phongService } from '../../../service/phongService'
import { message } from 'antd'

export default function UpdateRoom() {
    const { id } = useParams()
    const navigate = useNavigate()

    const onFinish = (values) => {
        phongService.putPhong({ values, id }).then((result) => {
            message.success('Cập nhật thành công!')
            navigate('/admin/quanlyphong')

        }).catch((err) => {
            message.error('Cập nhật thất bại!')
        });
    }

    return (
        <div>
            <h1 className='text-xl font-semibold text-center'>Chỉnh sửa thông tin phòng</h1>
            <FormInforPhong onFinish={onFinish} msgBtn="Cập nhật" />
        </div>
    )
}
