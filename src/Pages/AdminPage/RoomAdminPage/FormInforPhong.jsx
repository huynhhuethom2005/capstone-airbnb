import React, { useEffect } from 'react'
import { Button, Form, Input, InputNumber, Select, Space, Switch } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import { useState } from 'react';
import { vitriService } from '../../../service/vitriService';
import { useSelector } from 'react-redux';

export default function FormInforPhong({ onFinish, msgBtn }) {
    const [vitri, setVitri] = useState([])
    const { formValues } = useSelector((state) => state.adminPhongSlice)
   
    const renderVitriOption = () => {
        return vitri.map(item => {
            return (<Select.Option value={item.id}>{item.tenViTri} - {item.tinhThanh}</Select.Option>)
        })
    }

    useEffect(() => {
        vitriService.getVitri().then((result) => {
            setVitri(result.data.content)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }, [])


    return (
        <>
            <Form
                layout="vertical"
                onFinish={onFinish}
                initialValues={formValues}
            >
                <Form.Item label="Tên phòng" name="tenPhong">
                    <Input />
                </Form.Item>
                <Space className='space-x-10'>
                    <Form.Item label="Số lượng khách" name="khach"
                        rules={[{ type: 'number', min: 1, max: 99 }]}>
                        <InputNumber />
                    </Form.Item>
                    <Form.Item label="Số lượng phòng ngủ" name="phongNgu"
                        rules={[{ type: 'number', min: 1, max: 99 }]}>
                        <InputNumber />
                    </Form.Item>
                    <Form.Item label="Số lượng giường" name="giuong">
                        <InputNumber />
                    </Form.Item>
                    <Form.Item label="Số lượng phòng tắm" name="phongTam"
                        rules={[{ type: 'number', min: 1, max: 99 }]}>
                        <InputNumber />
                    </Form.Item>
                    <Form.Item label="Giá tiền ($)" name="giaTien"
                        rules={[{ type: 'number', min: 1, max: 99 }]}>
                        <InputNumber />
                    </Form.Item>
                </Space>
                <Form.Item label="Mô tả" name="moTa">
                    <TextArea rows={4} />
                </Form.Item>
                <Space className='space-x-14'>
                    <Form.Item label="Máy giặt" valuePropName="checked" name="mayGiat">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Bàn là" valuePropName="checked" name="banLa">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Tivi" valuePropName="checked" name="tivi">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Điều hoà" valuePropName="checked" name="dieuHoa">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Wifi" valuePropName="checked" name="wifi">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Bếp" valuePropName="checked" name="bep">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Đỗ xe" valuePropName="checked" name="doXe">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Hồ bơi" valuePropName="checked" name="hoBoi">
                        <Switch />
                    </Form.Item>
                    <Form.Item label="Bàn ủi" valuePropName="checked" name="banUi">
                        <Switch />
                    </Form.Item>
                </Space>
                <Form.Item label="Vị trí" name="maViTri">
                    <Select>
                        {renderVitriOption()}
                    </Select>
                </Form.Item>
                <Form.Item >
                    <Button htmlType="submit">{msgBtn}</Button>
                </Form.Item>
            </Form>
        </>
    )
}
