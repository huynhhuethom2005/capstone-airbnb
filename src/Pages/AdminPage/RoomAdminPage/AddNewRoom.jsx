import React, { useEffect } from 'react'

import { phongService } from '../../../service/phongService';
import { DOMAIN_URL } from '../../../service/config';
import FormInforPhong from './FormInforPhong';
import { message } from 'antd';
import { useNavigate } from 'react-router-dom';

export default function AddNewRoom() {
    const navigate = useNavigate()
    const onFinish = (values) => {
        phongService.postPhong(values).then((result) => {
            navigate('/admin/quanlyphong')
            message.success("Thêm mới thành công!")
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    };

    return (
        <div>
            <h1 className='text-xl font-semibold text-center'>Thêm thông tin phòng mới</h1>
            <FormInforPhong onFinish={onFinish} msgBtn="Thêm phòng mới" />
        </div>
    )
}
