import React, { useEffect, useState } from 'react'
import { adminUserServ } from '../../../service/adminUserService';
import { columns } from './utils';
import { Button, DatePicker, Form, Input, Modal, Select, Space, Table, message } from 'antd';

export default function UserAdminPage() {
    const [user, setUser] = useState([])
    const [isModalDeleteOpen, setIsModalDeleteOpen] = useState(false);
    const [isModalAddOpen, setIsModalAddOpen] = useState(false);
    const [idDelete, setIdDelete] = useState();
    const fetchUser = () => {
        adminUserServ.getAllUser().then((result) => {
            setUser(result.data.content)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }

    const showModalDelete = (id) => {
        setIdDelete(id)
        setIsModalDeleteOpen(true);
    };

    const showModalAdd = () => {
        setIsModalAddOpen(true);
    };

    const actionBtn = [
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: 150,
            render: (_, record) => (
                <Space size="middle">
                    <button onClick={() => { showModalDelete(record.id) }} type="button" className='rounded bg-red-500 p-2 text-white hover:bg-red-200 transition'>Xoá</button>
                    <button type="button" className='rounded bg-yellow-400 p-2 text-white hover:bg-yellow-200 transition'>Sửa</button>
                </Space >
            ),
        }
    ];

    const handleOkDelete = () => {
        adminUserServ.deleteUser(idDelete).then((result) => {
            fetchUser()
            message.success('Xóa người dùng thành công')
        }).catch((err) => {
            console.log('err :>> ', err);
            message.error('Xóa người dùng thất bại')
        });
        setIsModalDeleteOpen(false);

    };
    const onFinish = (values) => {
        adminUserServ.createUser(values).then((result) => {
            message.success('Thêm người dùng thành công')
            setIsModalAddOpen(false);
        }).catch((err) => {
            console.log('err :>> ', err);
            message.error('Thêm người dùng thất bại')
        });
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const handleCancelDelete = () => {
        setIsModalDeleteOpen(false);
    };

    useEffect(() => {
        fetchUser()
    }, [])
    console.log(user)

    const columnsClone = [...columns, ...actionBtn]
    return (
        <div>
            <Button type='primary' onClick={setIsModalAddOpen} danger > Thêm người dùng</Button>
            <Table columns={columnsClone} dataSource={user} scroll={{ x: 1000, y: 650 }} />

            <Modal title="Xoá dữ liệu" open={isModalDeleteOpen} 
            onOk={handleOkDelete}
            onCancel={handleCancelDelete}>
                <p>Bạn có chắc muốn xóa người dùng này không ?</p>
            </Modal>
            <Modal title="Thêm người dùng" open={isModalAddOpen} footer={null}>
                <Form
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="Tên người dùng"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Tên người dùng không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Điền tên vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Email không được để trống!',
                            },
                        ]}
                    >
                        <Input placeholder='Example@gmail.com' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Số điện thoại"
                        name="phone"
                    >
                        <Input placeholder='098 6888 234' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Mật khẩu"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Mật khẩu không được để trống!',
                            },
                        ]}
                    >
                        <Input.Password placeholder='*********' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Địa chỉ"
                        name="address"
                    >
                        <Input placeholder='Điền địa chỉ vào đây...' className='p-2' />
                    </Form.Item>
                    <Form.Item
                        label="Ngày sinh"
                        name="birthday"
                    >
                        <DatePicker placeholder='mm/dd/yyyy' className='p-2 w-full' />
                    </Form.Item>
                    <Form.Item
                        label="Giới tính"
                        name="gender"
                    >
                        <Select placeholder='----Chọn giới tính----'>
                            <Select.Option value="true">Nam</Select.Option>
                            <Select.Option value="false">Nữ</Select.Option>
                        </Select>

                    </Form.Item>
                    <Form.Item
                        label="Quyền"
                        name="role"
                    >
                        <Select placeholder='----Chọn quyền----'>
                            <Select.Option value="ADMIN">Quản trị viên</Select.Option>
                            <Select.Option value="USER">Người dùng</Select.Option>
                        </Select>

                    </Form.Item>
                    <Form.Item>
                        <div className='flex justify-around items-center '>
                            <Button className='w-1/2 h-10 rounded-full' type="primary" danger htmlType="submit">Thêm người dùng</Button>
                        </div>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}
