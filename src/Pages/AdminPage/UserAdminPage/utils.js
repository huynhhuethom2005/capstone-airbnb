import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Space, Table, Tag } from 'antd';
import { Image } from 'antd';

const columns = [
    {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        width: 100,
        fixed: 'left',
    },
    {
        title: 'Tên người dùng',
        dataIndex: 'name',
        key: 'name',
        width: 200,
        fixed: 'left',
    },
    {
        title: 'Ngày sinh',
        dataIndex: 'birthday',
        key: 'birthday',
        width: 200,
        fixed: 'left',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        width: 100,
    },
    {
        title: 'Số điện thoại',
        dataIndex: 'phone',
        key: 'phone',
        width: 100,
    },
    {
        title: 'Giới tính',
        dataIndex: 'gender',
        key: 'gender',
        width: 100,
        render: (gender, _) => (<div>{gender ? <Tag color='green' >
           Nam
        </Tag> : <Tag color='volcano' >
           Nữ
        </Tag>}</div>)
    },
    {
        title: 'Quyền',
        dataIndex: 'role',
        key: 'role',
        width: 100,
        render: (role, _) => (<div>{role =='ADMIN' ? <Tag color='red' >
            Quản trị viên
        </Tag> : <Tag color='green' >
            Người dùng
        </Tag>}</div>)
    },
];

export { columns }