export const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 100,
    fixed: "left",
  },
  {
    title: "Tên vị trí",
    dataIndex: "tenViTri",
    key: "tenViTri",
    width: 200,
    fixed: "left",
  },
  {
    title: "Tỉnh thành",
    dataIndex: "tinhThanh",
    key: "tinhThanh",
    width: 100,
  },
  {
    title: "Quốc gia",
    dataIndex: "quocGia",
    key: "quocGia",
    width: 100,
  },
];
