import React, { useEffect, useState } from "react";
import { vitriService } from "../../../service/vitriService";
import { columns } from "./utils";
import { Button, Space, Table, Modal, message } from "antd";

export default function LocationAdminPage() {
  const [location, setLocation] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [currentId, setCurrentId] = useState();

  const showDeleteModal = (id) => {
    setDeleteModal(true);
    setCurrentId(id);
  };

  const hideDeleteModal = () => {
    setDeleteModal(false);
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleDelete = (id) => {
    vitriService
      .deleteViTri(id)
      .then((res) => {
        console.log(res);
        message.info("Đã xóa ", res.data.content);
        fetchLocation();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleAdd = () => {};

  const fetchLocation = () => {
    vitriService
      .getVitri()
      .then((res) => {
        console.log(res.data.content);
        setLocation(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const actionBtn = [
    {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 150,
      render: (_, record) => (
        <Space size="middle">
          <button
            type="button"
            onClick={() => {
              showDeleteModal(record);
            }}
            className="rounded bg-red-500 p-2 text-white hover:bg-red-200 transition"
          >
            Xoá
          </button>
          <button
            type="button"
            className="rounded bg-yellow-400 p-2 text-white hover:bg-yellow-200 transition"
          >
            Sửa
          </button>
        </Space>
      ),
    },
  ];

  const columnsClone = [...columns, ...actionBtn];

  useEffect(() => {
    fetchLocation();
  }, []);
  return (
    <div>
      {/* <Modal
        title="Basic Modal"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
      <div className=" flex justify-end">
        <Button
          onClick={showModal}
          type="primary"
          size={"large"}
          className=" relative bg-blue-600 right-0"
        >
          Add Location
        </Button>
      </div> */}
      {/* delete Modal  */}
      <Modal
        title="Modal"
        open={deleteModal}
        onOk={handleDelete(currentId)}
        onCancel={hideDeleteModal}
        okText="Yes?"
        cancelText="No"
      >
        <p>Bạn có muốn xóa vị trí này không?</p>
      </Modal>

      <Table
        columns={columnsClone}
        dataSource={location}
        scroll={{ x: 1000, y: 650 }}
      />
    </div>
  );
}
