import { createSlice } from '@reduxjs/toolkit'
import { localServ } from '../service/localService';

const initialState = {
    // initial Value
    userLogin: null
}

const userSlice = createSlice({
    name: 'userSlice',
    initialState,
    reducers: {
        setUserLogin: (state, action) => {
            state.userLogin = action.payload.data.content.user;
            localServ.set(action.payload.data.content)
        },
    }
});

export const { setUserLogin } = userSlice.actions

export default userSlice.reducer