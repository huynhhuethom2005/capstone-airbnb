import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    formValues: null
}

const adminPhongSlice = createSlice({
    name: 'adminPhongSlice',
    initialState,
    reducers: {
        setFormValue: (state, action) => {
            state.formValues = action.payload
        }
    }
});

export const { setFormValue } = adminPhongSlice.actions

export default adminPhongSlice.reducer