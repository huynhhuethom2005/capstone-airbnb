import { DesktopOutlined, FileOutlined, GlobalOutlined, PieChartOutlined, TeamOutlined, UserOutlined, UserSwitchOutlined } from '@ant-design/icons';
import { Breadcrumb, Layout, Menu, theme } from 'antd';
import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
// import { localUserSer } from '../service/localService';
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
    return {
        key,
        icon,
        children,
        label,
    };
}
const items = [
    getItem(<NavLink to="/admin" >Quản lý Phòng</NavLink>, '1', <PieChartOutlined />),
    getItem(<NavLink to="/admin/quanlyvitri" >Quản lý Vị trí</NavLink>, '2', <GlobalOutlined />),
    getItem(<NavLink to="/admin/quanlynguoidung" >Quản lý Người dùng</NavLink>, '3', <UserSwitchOutlined />),
];

const AdminLayout = ({ Component }) => {
    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    // useEffect(() => {
    //     if (localUserSer.get()?.maLoaiNguoiDung != "QuanTri") {
    //         window.location.href = "/login-page"
    //     }
    // }, [])
    return (
        <Layout
            style={{
                minHeight: '100vh',
            }}
        >
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        height: 32,
                        margin: 16,
                        background: 'rgba(255, 255, 255, 0.2)',
                    }}
                >
                    CAPSTONE AIRBNB
                </div>
                <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} />
            </Sider>
            <Layout className="site-layout">
                <Header
                    style={{
                        padding: 0,
                        background: colorBgContainer,
                    }}
                />
                <Content
                    style={{
                        margin: '0 16px',
                    }}
                >
                    <div
                        style={{
                            padding: 24,
                            minHeight: 360,
                            background: colorBgContainer,
                        }}
                    >
                        <Component />
                    </div>
                </Content>
                <Footer
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Admin airbnb react
                </Footer>
            </Layout>
        </Layout>
    );
};
export default AdminLayout;