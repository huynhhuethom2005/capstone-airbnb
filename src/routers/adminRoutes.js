import AdminLayout from "../Layout/LayoutAdmin";
import LocationAdminPage from "../Pages/AdminPage/LocationAdminPage/LocationAdminPage";
import AddNewRoom from "../Pages/AdminPage/RoomAdminPage/AddNewRoom";
import RoomAdminPage from "../Pages/AdminPage/RoomAdminPage/RoomAdminPage";
import UpdateRoom from "../Pages/AdminPage/RoomAdminPage/UpdateRoom";
import UserAdminPage from "../Pages/AdminPage/UserAdminPage/UserAdminPage";

export const adminRoutes = [
    {
        url: "/admin",
        component: <AdminLayout Component={RoomAdminPage} />
    },
    {
        url: "/admin/quanlyphong",
        component: <AdminLayout Component={RoomAdminPage} />
    },
    {
        url: "/admin/quanlyphong/newroom",
        component: <AdminLayout Component={AddNewRoom} />
    },
    {
        url: "/admin/quanlyphong/update/:id",
        component: <AdminLayout Component={UpdateRoom} />
    },
    {
        url: "/admin/quanlyvitri",
        component: <AdminLayout Component={LocationAdminPage} />
    },
    {
        url: "/admin/quanlynguoidung",
        component: <AdminLayout Component={UserAdminPage} />
    },
]