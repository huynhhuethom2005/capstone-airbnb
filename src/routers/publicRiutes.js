import Layout from "../Layout/Layout";
import DetailPage from "../Pages/PublicPage/DetailPage/DetailPage";
import HomePage from "../Pages/PublicPage/HomePage/HomePage";
import RoomByCityPage from "../Pages/PublicPage/RoomByCityPage/RoomByCityPage";

export const publicRoutes = [
    {
        url: '/',
        component: <Layout Component={HomePage} />,
    },
    {
        url: '/detail/:id',
        component: <Layout Component={DetailPage} />,
    },
    {
        url: "/roombycity/:id",
        component: <Layout Component={RoomByCityPage} />,
    }
]