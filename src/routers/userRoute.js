import Layout from "../Layout/Layout";
import LoginPage from "../Pages/PublicPage/LoginPage/LoginPage";
import PersonalInfoPage from "../Pages/PublicPage/PersonalInfoPage/PersonalInfoPage";
import RegisterPage from "../Pages/PublicPage/RegisterPage/RegisterPage";

export const userRoutes = [
  {
    url: '/login',
    component: <LoginPage />,
  },
  {
    url: '/register',
    component: <RegisterPage />,
  },
  {
    url: '/personal-info/:id',
    component: <Layout Component={PersonalInfoPage} />,
  }
]
